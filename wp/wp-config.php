<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'curto_guitarra');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'B(^L84Z%F*7`{j}3:e;F|Xz4 N#Y{,X!:*!F|AOwE+)HuaievYiUsLE=yh$dVj1c');
define('SECURE_AUTH_KEY',  'ge#2<FuB+]}W|wt.k^^XFP%O!2s=:DE9<Hg--n&~x,Aer u~$K>P(<kwP7/p.:!i');
define('LOGGED_IN_KEY',    'fiw k$x>1f#+vZew9HYgI/A=&dJDfgIIMxorO2_EERCF?~AA#72K_5)6cFqz?--/');
define('NONCE_KEY',        'u[L 7y-Tkw?~R0+]*;Fo_#*Fl|~DeJIY:WkMo|kN]^?[^i!|Xv4[UwdY*Gmxn@Ub');
define('AUTH_SALT',        'F`>|Ynd*V4}l:mQ{)r<};8H-IC]:JS1Kuv0HEBm.oz~2/P2sce?hn:@/!(J]}Lji');
define('SECURE_AUTH_SALT', '~k>q=H@L3TZB%[igiC||*SdqT2??L}ic.u_M?3OnSis4cXm^a|^|hn12Fi|l!vjy');
define('LOGGED_IN_SALT',   '_igBdH1Q$&Wuh![O+.tQUL!-KLFSW{ +f3)e`%v)- <s[}m90=PI_J+uK@1ii1Xb');
define('NONCE_SALT',       '4AM-K!6pSzH&(fAU}F)%-]-gP!/9Y@z!0hZ xd,uNlFz6k@qW:7B;g7vA<yV,SP%');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
