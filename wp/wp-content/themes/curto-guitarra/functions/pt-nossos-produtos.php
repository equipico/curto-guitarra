<?php
function register_post_type_nossos_produtos() {
	register_post_type( 'nossos_produtos',
		array(
			'labels' => array(
				'name' => __( 'Nossos Produtos' ),
				'singular_name' => __( 'Nosso Produto' )
			),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'nossos-produtos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
		)
	);
}
add_action( 'init', 'register_post_type_nossos_produtos' );