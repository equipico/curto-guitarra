<?php
function register_post_type_conheca_leandro() {
	register_post_type( 'conheca_leandro',
		array(
			'labels' => array(
				'name' => __( 'Conheça Leandro' ),
				'singular_name' => __( 'Conheça Leandro' )
			),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'conheca-leandro' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'thumbnail' )
		)
	);
}
add_action( 'init', 'register_post_type_conheca_leandro' );