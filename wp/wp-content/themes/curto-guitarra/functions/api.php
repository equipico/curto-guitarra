<?php
function api_presets() {
	header("Access-Control-Allow-Origin: *; Content-Encoding: gzip; Content-Type: application/json; charset=utf-8");
	ob_start('ob_gzhandler');
}

function get_menu() {
	api_presets();

	$menu_items = wp_get_nav_menu_items('menu');
	$array_return = array();

	foreach ($menu_items as $key => $item) {
		$obj = new stdClass();
		if ($item->object == 'page') {
			$obj->title = $item->title;
			$obj->link = substr(strrchr(substr($item->url, 0, strlen($item->url)-1), '/'), 1);
		} else {
			$obj->title = $item->post_title;
			$obj->link = $item->url;
		}
		$array_return[] = $obj;
	}

	echo json_encode($array_return);

    die();
}
add_action( 'wp_ajax_get_menu', 'get_menu' );
add_action( 'wp_ajax_nopriv_get_menu', 'get_menu' );

function get_conheca_leandro() {
	api_presets();

	$conheca_leandro = get_posts(array(
		'posts_per_page'   => 1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'conheca_leandro',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	));

	foreach ($conheca_leandro as $key => $leandro) {
		$conheca_leandro[$key]->thumb = wp_get_attachment_image_src(get_post_thumbnail_id($leandro->ID), 'conheca-fernando-thumbnail');
	}

	$conheca_leandro = count($conheca_leandro)>0 ? $conheca_leandro[0] : $conheca_leandro;

	echo json_encode($conheca_leandro);

    die();
}
add_action( 'wp_ajax_get_conheca_leandro', 'get_conheca_leandro' );
add_action( 'wp_ajax_nopriv_get_conheca_leandro', 'get_conheca_leandro' );

function get_blog_configuration() {
	api_presets();

	$customization = get_posts(array(
		'posts_per_page'   => 1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'blog_customization',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	));

	foreach ($customization as $key => $cust) {
		$customization[$key]->tipo_topo = get_post_meta( $cust->ID, '_tipo_topo', true );
		$customization[$key]->tipo_conteudo = get_post_meta( $cust->ID, '_tipo_conteudo', true );
		$customization[$key]->link_facebook = get_post_meta( $cust->ID, '_link_facebook', true );
		$customization[$key]->link_instagram = get_post_meta( $cust->ID, '_link_instagram', true );
		$customization[$key]->link_youtube = get_post_meta( $cust->ID, '_link_youtube', true );
		$customization[$key]->link_twitter = get_post_meta( $cust->ID, '_link_twitter', true );
		$customization[$key]->link_video_youtube = get_post_meta( $cust->ID, '_link_video_youtube', true );
		$customization[$key]->call_to_action_activation = get_post_meta( $cust->ID, '_call_to_action_activation', true );
		$customization[$key]->call_to_action_text = get_post_meta( $cust->ID, '_call_to_action_text', true );
		$customization[$key]->call_to_action_text_2 = get_post_meta( $cust->ID, '_call_to_action_text_2', true );
		$customization[$key]->call_to_action_form_code = get_post_meta( $cust->ID, '_call_to_action_form_code', true );
		$customization[$key]->call_to_action_thumb_url = get_post_meta( $cust->ID, '_call_to_action_thumb_url', true );
	}

	$customization = count($customization)>0 ? $customization[0] : $customization;

	echo json_encode($customization);

    die();
}
add_action( 'wp_ajax_get_blog_configuration', 'get_blog_configuration' );
add_action( 'wp_ajax_nopriv_get_blog_configuration', 'get_blog_configuration' );

function get_produtos() {
	api_presets();

	$nossos_produtos = get_posts(array(
		'posts_per_page'   => 6,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'nossos_produtos',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	));

	foreach ($nossos_produtos as $key => $produto) {
		$nossos_produtos[$key]->thumb = wp_get_attachment_image_src(get_post_thumbnail_id($produto->ID), 'product-thumbnail');
	}

	echo json_encode($nossos_produtos);

    die();
}
add_action( 'wp_ajax_get_produtos', 'get_produtos' );
add_action( 'wp_ajax_nopriv_get_produtos', 'get_produtos' );

function get_post_by_slug() {
	api_presets();

	$post_type = isset($_REQUEST['post_type']) ? $_REQUEST['post_type'] : 'post';
	$slug = isset($_REQUEST['slug']) ? $_REQUEST['slug'] : '' ;

	$posts = get_posts(array(
		'posts_per_page'   => 1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'name'       	   => $slug,
		'post_type'        => $post_type,
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	));

	foreach ($posts as $key => $post) {
		$posts[$key]->thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'product-thumbnail');
		$posts[$key]->thumb_full = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
		$posts[$key]->post_content = apply_filters('the_content', $post->post_content);
	}

	$posts = count($posts)>0 ? $posts[0] : $posts;

	echo json_encode($posts);

    die();
}
add_action( 'wp_ajax_get_post_by_slug', 'get_post_by_slug' );
add_action( 'wp_ajax_nopriv_get_post_by_slug', 'get_post_by_slug' );

function get_news() {
	api_presets();

	$args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	);

	if (isset($_REQUEST['posts_per_page']) && strlen($_REQUEST['posts_per_page'])>0) {
		$args['posts_per_page'] = $_REQUEST['posts_per_page'];
	}

	if (isset($_REQUEST['post_type']) && strlen($_REQUEST['post_type'])>0) {
		$args['post_type'] = $_REQUEST['post_type'];
	}

	if (isset($_REQUEST['offset']) && strlen($_REQUEST['offset'])>0) {
		$args['offset'] = $_REQUEST['offset'];
	}

	$news = get_posts($args);

	foreach ($news as $key => $post) {
		$news[$key]->thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'news-thumbnail');
		$news[$key]->thumb_slide = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'slide-thumbnail');
		$news[$key]->thumb_slide_only_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'slide-without-text-thumbnail');

		$author = get_user_by('ID', $post->post_author);
		$news[$key]->author = isset($author->data) ? $author->data->user_nicename : $author;

		$news[$key]->categories = get_the_terms($post->ID, 'category');
		$date = strtotime($post->post_date_gmt);
		$news[$key]->post_date_formated = date('d' ,$date).' de '.date('F' ,$date).' de '.date('Y' ,$date);
	}

	echo json_encode($news);

    die();
}
add_action( 'wp_ajax_get_news', 'get_news' );
add_action( 'wp_ajax_nopriv_get_news', 'get_news' );

function get_slides() {
	api_presets();

	$slides = get_posts(array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'slide',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	));

	foreach ($slides as $key => $slide) {
		$slides[$key]->thumb = wp_get_attachment_image_src(get_post_thumbnail_id($slide->ID), 'slide-thumbnail');
		$slides[$key]->thumb_slide = wp_get_attachment_image_src(get_post_thumbnail_id($slide->ID), 'slide-without-text-thumbnail');
		$slides[$key]->link = get_post_meta( $slide->ID, '_link', true );
	}

	echo json_encode($slides);

    die();
}
add_action( 'wp_ajax_get_slides', 'get_slides' );
add_action( 'wp_ajax_nopriv_get_slides', 'get_slides' );

function get_slides_without_text() {
	api_presets();

	$slides = get_posts(array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'slide-without-text',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	));

	foreach ($slides as $key => $slide) {
		$slides[$key]->thumb = wp_get_attachment_image_src(get_post_thumbnail_id($slide->ID), 'slide-thumbnail');
		$slides[$key]->thumb_slide = wp_get_attachment_image_src(get_post_thumbnail_id($slide->ID), 'slide-without-text-thumbnail');
		$slides[$key]->link = get_post_meta( $slide->ID, '_link', true );
	}

	echo json_encode($slides);

    die();
}
add_action( 'wp_ajax_get_slides_without_text', 'get_slides_without_text' );
add_action( 'wp_ajax_nopriv_get_slides_without_text', 'get_slides_without_text' );

function get_news_categories() {
	api_presets();

	$categories = get_terms(
		'category',
		array(
			'orderby'           => 'name', 
			'order'             => 'ASC',
			'hide_empty'        => true, 
			'exclude'           => array(), 
			'exclude_tree'      => array(), 
			'include'           => array(),
			'number'            => '', 
			'fields'            => 'all', 
			'slug'              => '',
			'parent'            => '',
			'hierarchical'      => true, 
			'child_of'          => 0,
			'childless'         => false,
			'get'               => '', 
			'name__like'        => '',
			'description__like' => '',
			'pad_counts'        => false, 
			'offset'            => '', 
			'search'            => '', 
			'cache_domain'      => 'core'
		)
	);

	echo json_encode($categories);

    die();
}
add_action( 'wp_ajax_get_news_categories', 'get_news_categories' );
add_action( 'wp_ajax_nopriv_get_news_categories', 'get_news_categories' );

function get_term_by_slug() {
	api_presets();

	$posts = get_posts(array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => $_REQUEST['slug'],
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	));

	echo json_encode($posts);

    die();
}
add_action( 'wp_ajax_get_term_by_slug', 'get_term_by_slug' );
add_action( 'wp_ajax_nopriv_get_term_by_slug', 'get_term_by_slug' );

function get_search_results() {
	api_presets();

	$searchWord = isset($_REQUEST['search_string']) ? $_REQUEST['search_string'] : '';
	$noticias = get_posts(array(
		'posts_per_page'    => -1,
		'offset'            => 0,
		'orderby'           => 'post_date',
		'order'             => 'DESC',
		'include'           => '',
		'exclude'           => '',
		'cat'               => '',
		'meta_key'          => '',
		'meta_value'        => '',
		'post_type'         => 'post',
		'post_mime_type'    => '',
		'post_parent'       => '',
		'post_status'       => 'publish',
		'suppress_filters'  => true
	));
	$posts = array();
	foreach($noticias as $key => $noticia){
		if( is_int(strripos($noticia->post_title, $searchWord)) || is_int(strripos($noticia->post_content, $searchWord)) || is_int(strripos($noticia->post_excerpt, $searchWord)) ){
			$posts[]=$noticia;
		}
	}

	echo json_encode($posts);

    die();
}
add_action( 'wp_ajax_get_search_results', 'get_search_results' );
add_action( 'wp_ajax_nopriv_get_search_results', 'get_search_results' );

function get_thumbnail_by_id() {
	api_presets();

	$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
	$size = isset($_REQUEST['size']) ? $_REQUEST['size'] : 'full';
	
	echo json_encode(wp_get_attachment_image_src($id, $size));

    die();
}
add_action( 'wp_ajax_get_thumbnail_by_id', 'get_thumbnail_by_id' );
add_action( 'wp_ajax_nopriv_get_thumbnail_by_id', 'get_thumbnail_by_id' );

