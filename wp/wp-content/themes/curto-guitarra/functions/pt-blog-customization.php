<?php
function register_post_type_blog_customization() {
	register_post_type( 'blog_customization',
		array(
			'labels' => array(
				'name' => __( 'Configurações do Blog' ),
				'singular_name' => __( 'Configuração do Blog' )
			),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'configuracao-blog' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title' )
		)
	);
}
add_action( 'init', 'register_post_type_blog_customization' );

/** Custom Fields box **/
function custom_post_blog_customization_add_meta_box() {

	add_meta_box(
		'custom_post_blog_customization_data',
		__( 'Opções', 'myplugin_textdomain' ),
		'custom_post_blog_customization_meta_box_callback',
		'blog_customization'
	);

}
add_action( 'add_meta_boxes', 'custom_post_blog_customization_add_meta_box' );

/** Custom Fields box html **/
function custom_post_blog_customization_meta_box_callback( $post ) {
	// Add an nonce field so we can check for it later.
	wp_nonce_field( 'custom_post_blocos_texto_meta_box', 'custom_post_blocos_texto_meta_box_nonce' );
	$attachments = get_posts(array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		// 'category'         => '',
		// 'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		// 'meta_key'         => '',
		// 'meta_value'       => '',
		'post_type'        => 'attachment',
		'post_mime_type'   => '',
		'post_parent'      => $post->ID,
		// 'author'	   	   => '',
		// 'post_status'      => 'publish',
		// 'suppress_filters' => true 
	));

	$tipo_topo = get_post_meta( $post->ID, '_tipo_topo', true );
	$tipo_conteudo = get_post_meta( $post->ID, '_tipo_conteudo', true );
	$link_facebook = get_post_meta( $post->ID, '_link_facebook', true );
	$link_instagram = get_post_meta( $post->ID, '_link_instagram', true );
	$link_youtube = get_post_meta( $post->ID, '_link_youtube', true );
	$link_twitter = get_post_meta( $post->ID, '_link_twitter', true );
	$link_video_youtube = get_post_meta( $post->ID, '_link_video_youtube', true );
	$call_to_action_activation = get_post_meta( $post->ID, '_call_to_action_activation', true );
	$call_to_action_text = get_post_meta( $post->ID, '_call_to_action_text', true );
	$call_to_action_text_2 = get_post_meta( $post->ID, '_call_to_action_text_2', true );
	$call_to_action_form_code = get_post_meta( $post->ID, '_call_to_action_form_code', true );
	$call_to_action_thumb = get_post_meta( $post->ID, '_call_to_action_thumb', true );
	$call_to_action_thumb_url = get_post_meta( $post->ID, '_call_to_action_thumb_url', true );

	$thumb_img = wp_get_attachment_image_src($call_to_action_thumb, 'call-to-action-thumbnail');
?>
	<style>
		#meta-box-fields-list{
			width: 100%;
		}
		#meta-box-fields-list label {
			display: block;
			font-size: 1.3em;
		}
		#meta-box-fields-list label > img {
			max-width: 100%;
			margin-bottom: 10px;
		}
		#meta-box-fields-list strong {
			font-size: 1.4em;
			margin-bottom: 7px;
			display: inline-block;
		}
		#meta-box-fields-list textarea {
			width: 100%;
			height: 150px;
		}
	</style>
	<table id="meta-box-fields-list">
		<tbody>
			<tr>
				<td>
					<strong>Tipo de topo</strong>
					<label><input type="radio" name="tipo_topo" value="1" <?php echo !isset($tipo_topo) || strlen($tipo_topo)==0 || $tipo_topo == 1 ? 'checked=""' : '' ; ?>> Slide com imagem de fundo fixa <br> <img src="<?php bloginfo('template_url'); ?>/functions/imgs/slide-img-fixed.png" alt="Topo fixo" width="350"></label>
					<label><input type="radio" name="tipo_topo" value="2" <?php echo $tipo_topo == 2 ? 'checked=""' : '' ; ?>> Topo com slide de fotos(Adicionar ao menos 2 slides)  <br> <img src="<?php bloginfo('template_url'); ?>/functions/imgs/slide-video.png" alt="Topo slide" width="350"></label>
					<label><input type="radio" name="tipo_topo" value="3" <?php echo $tipo_topo == 3 ? 'checked=""' : '' ; ?>> Topo com ultimos posts <br> <img src="<?php bloginfo('template_url'); ?>/functions/imgs/slide-video.png" alt="Topo noticias" width="350"></label>
					<hr>
				</td>
			</tr>
			<tr>
				<td>
					<strong>Layout do conteúdo</strong>
					<label><input type="radio" name="tipo_conteudo" value="1" <?php echo !isset($tipo_conteudo) || strlen($tipo_conteudo)==0 || $tipo_conteudo == 1 ? 'checked=""' : '' ; ?>> Com sidebar</label>
					<label><input type="radio" name="tipo_conteudo" value="2" <?php echo $tipo_conteudo == 2 ? 'checked=""' : '' ; ?>> Sem sidebar</label>

					<hr>
				</td>
			</tr>
			<!-- <tr>
				<td>
					<label>
						Link do vídeo do youtube do fundo do slide (somente o id do vídeo) <br>
						<input type="text" name="link_video_youtube" value="<?php //echo $link_video_youtube; ?>">
					</label>
				</td>
			</tr> -->
			<tr>
				<td>
					<strong>Redes Sociais</strong>
					<label>
						Link do Facebook <br>
						<input type="text" name="link_facebook" value="<?php echo $link_facebook; ?>">
					</label>
					<label>
						Link do Instagram <br>
						<input type="text" name="link_instagram" value="<?php echo $link_instagram; ?>">
					</label>
					<label>
						Link do Youtube <br>
						<input type="text" name="link_youtube" value="<?php echo $link_youtube; ?>">
					</label>
					<label>
						Link do Twitter <br>
						<input type="text" name="link_twitter" value="<?php echo $link_twitter; ?>">
					</label>

					<hr>
				</td>
			</tr>
			<tr>
				<td>
					<strong>Call to Action</strong>
					<label>
						<input type="radio" name="call_to_action_activation" value="true" <?php echo isset($call_to_action_activation) && $call_to_action_activation == 'true' ? 'checked' : '' ; ?>> Ativado
					</label>
					<label>
						<input type="radio" name="call_to_action_activation" value="false" <?php echo isset($call_to_action_activation) && strlen($call_to_action_activation)==0 || $call_to_action_activation == 'false' ? 'checked' : '' ; ?>> Desativado
					</label>
					
					<label>Texto</label>
					<?php wp_editor( $call_to_action_text, 'call_to_action_text' ); ?>
					<label>Segundo campo de Texto</label>
					<?php wp_editor( $call_to_action_text_2, 'call_to_action_text_2' ); ?>
					<label>Código do formulário</label>
					<textarea name="call_to_action_form_code"><?php echo $call_to_action_form_code; ?></textarea>
					<div>
						<label>Imagem</label>
						<div class="wp-media-buttons"><button type="button" id="insert-media-button" class="button insert-media add_media" data-editor="call_to_action_text"><span class="wp-media-buttons-icon"></span> Adicionar Mídia</button></div>
						<?php if (isset($attachments) && count($attachments)>0) { ?>
							<div>
								<select name="call_to_action_thumb">
									<option value="">Selecione a imagem</option>
									<?php foreach ($attachments as $key => $attachment) { ?>
										<option value="<?= $attachment->ID; ?>" <?= $call_to_action_thumb == $attachment->ID ? 'selected' : ''; ?>><?= $attachment->post_title; ?></option>
									<?php } ?>
								</select>
								<input type="hidden" name="call_to_action_thumb_url" value="<?= isset($thumb_img[0]) ? $thumb_img[0] : ''; ?>" ?>
								<img src="<?= isset($thumb_img[0]) ? $thumb_img[0] : '' ?>" style="max-width:100%;" id="call_to_action_thumb_preview">
							</div>
							<script>
								jQuery('select[name="call_to_action_thumb"]').change(function(){
									if (jQuery(this).val()) {
										jQuery('#call_to_action_thumb_preview').show();

										jQuery.ajax({
											method: "GET",
											dataType: "json",
											url: 'admin-ajax.php',
											data: {
												action: 'get_thumbnail_by_id',
												id: jQuery(this).val(),
												size: 'call-to-action-thumbnail'
											},
											success: function(response){
												if (response && response[0]) {
													console.log(response[0]);
													jQuery('#call_to_action_thumb_preview').attr('src', response[0]);
													jQuery('input[name="call_to_action_thumb_url"]').attr('value', response[0]);
												}
											}
										});
									}else{
										jQuery('#call_to_action_thumb_preview').hide();
										jQuery('input[name="call_to_action_thumb_url"]').attr('value', '');
									}
									console.log();
								});
							</script>
						<?php } ?>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
<?php
}

/* Save and verify submited data of Field box */
function custom_post_blog_customization_save_meta_box_data( $post_id ) {
	// Check if our nonce is set.
	if ( ! isset( $_POST['custom_post_blocos_texto_meta_box_nonce'] ) ) {
		return;
	}
	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['custom_post_blocos_texto_meta_box_nonce'], 'custom_post_blocos_texto_meta_box' ) ) {
		return;
	}
	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	
	// Make sure that it is set.
	if ( ! isset( $_POST['tipo_topo'] ) ) {
		return;
	}

	// Sanitize user input.
	$tipo_topo = sanitize_text_field( $_POST['tipo_topo'] );
	$tipo_conteudo = sanitize_text_field( $_POST['tipo_conteudo'] );
	$link_facebook = sanitize_text_field( $_POST['link_facebook'] );
	$link_youtube = sanitize_text_field( $_POST['link_youtube'] );
	$link_instagram = sanitize_text_field( $_POST['link_instagram'] );
	$link_twitter = sanitize_text_field( $_POST['link_twitter'] );
	$link_video_youtube = sanitize_text_field( $_POST['link_video_youtube'] );
	$call_to_action_text = $_POST['call_to_action_text'];
	$call_to_action_text_2 = $_POST['call_to_action_text_2'];
	$call_to_action_form_code = $_POST['call_to_action_form_code'];
	$call_to_action_thumb = $_POST['call_to_action_thumb'];
	$call_to_action_thumb_url = $_POST['call_to_action_thumb_url'];
	$call_to_action_activation = $_POST['call_to_action_activation'];

	// Update the meta field in the database.
	update_post_meta( $post_id, '_tipo_topo', $tipo_topo );
	update_post_meta( $post_id, '_tipo_conteudo', $tipo_conteudo );
	update_post_meta( $post_id, '_link_facebook', $link_facebook );
	update_post_meta( $post_id, '_link_youtube', $link_youtube );
	update_post_meta( $post_id, '_link_instagram', $link_instagram );
	update_post_meta( $post_id, '_link_twitter', $link_twitter );
	update_post_meta( $post_id, '_link_video_youtube', $link_video_youtube );
	update_post_meta( $post_id, '_call_to_action_text', $call_to_action_text );
	update_post_meta( $post_id, '_call_to_action_text_2', $call_to_action_text_2 );
	update_post_meta( $post_id, '_call_to_action_form_code', $call_to_action_form_code );
	update_post_meta( $post_id, '_call_to_action_thumb', $call_to_action_thumb );
	update_post_meta( $post_id, '_call_to_action_thumb_url', $call_to_action_thumb_url );
	update_post_meta( $post_id, '_call_to_action_activation', $call_to_action_activation );
}

add_action( 'save_post', 'custom_post_blog_customization_save_meta_box_data' );