<?php
function register_post_type_slide() {
	register_post_type( 'slide',
		array(
			'labels' => array(
				'name' => __( 'Slides' ),
				'singular_name' => __( 'Slide' )
			),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'slide' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'thumbnail')
		)
	);
}
add_action( 'init', 'register_post_type_slide' );

/** Custom Fields box **/
function custom_post_slide_add_meta_box() {

	add_meta_box(
		'custom_post_slide_data',
		__( 'Opções', 'myplugin_textdomain' ),
		'custom_post_slide_meta_box_callback',
		'slide'
	);

}
add_action( 'add_meta_boxes', 'custom_post_slide_add_meta_box' );

/** Custom Fields box html **/
function custom_post_slide_meta_box_callback( $post ) {
	// Add an nonce field so we can check for it later.
	wp_nonce_field( 'custom_post_blocos_texto_meta_box', 'custom_post_blocos_texto_meta_box_nonce' );

	$link = get_post_meta( $post->ID, '_link', true );
?>
	<style>
		#meta-box-fields-list label {
			display: block;
			font-size: 1.3em;
		}
		#meta-box-fields-list label > img {
			max-width: 100%;
			margin-bottom: 10px;
		}
		#meta-box-fields-list strong {
			font-size: 1.4em;
			margin-bottom: 7px;
			display: inline-block;
		}
	</style>
	<table id="meta-box-fields-list">
		<tbody>
			<tr>
				<td>
					<strong>Link</strong>
					<label><input type="text" name="link" value="<?php echo $link; ?>"></label>
				</td>
			</tr>
		</tbody>
	</table>
<?php
}

/* Save and verify submited data of Field box */
function custom_post_slide_save_meta_box_data( $post_id ) {
	// Check if our nonce is set.
	if ( ! isset( $_POST['custom_post_blocos_texto_meta_box_nonce'] ) ) {
		return;
	}
	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['custom_post_blocos_texto_meta_box_nonce'], 'custom_post_blocos_texto_meta_box' ) ) {
		return;
	}
	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	
	// Make sure that it is set.
	if ( ! isset( $_POST['link'] ) ) {
		return;
	}

	// Sanitize user input.
	$link = sanitize_text_field( $_POST['link'] );

	// Update the meta field in the database.
	update_post_meta( $post_id, '_link', $link );
}

add_action( 'save_post', 'custom_post_slide_save_meta_box_data' );