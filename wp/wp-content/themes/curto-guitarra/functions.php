<?php
// theme support
add_theme_support('menus');
add_theme_support('post-thumbnails');

// Custom Posts
require_once('functions/pt-blog-customization.php');
require_once('functions/pt-slide.php');
require_once('functions/pt-slide-without-text.php');
require_once('functions/pt-conheca-leandro.php');
require_once('functions/pt-nossos-produtos.php');

// API
require_once('functions/api.php');

// Image size
add_image_size ('conheca-fernando-thumbnail', 280, 260, true);
add_image_size ('slide-thumbnail', 545, 470, false);
add_image_size ('call-to-action-thumbnail', 500, 350, false);
add_image_size ('product-thumbnail', 394, 251, true);
add_image_size ('news-thumbnail', 850, 542, true);
add_image_size( 'slide-without-text-thumbnail', 1903, 583, true );