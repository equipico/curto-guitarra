app.controller('SearchCtrl', ['$scope', '$http', '$stateParams', function ($scope, $http, $stateParams) {
	$scope.search_string = $stateParams.search;

	$http({
		method: 'GET',
		url: api_url+'?action=get_search_results&slug='+$stateParams.search,
	}).then(function successCallback(response){
		$scope.news = response.data;
	},
	function errorCallback(response){
		console.log('Erro ao carregar o resultado da busca...');
		console.log(response);
	})

	// pagination
	$scope.filteredNews = [];
	$scope.currentPage = 1;
	$scope.numPerPage = 3;
	$scope.maxSize = 5;

	$scope.$watch('currentPage + numPerPage', function() {
		var begin = (($scope.currentPage - 1) * $scope.numPerPage);
		var end = begin + $scope.numPerPage;

		$scope.filteredNews = $scope.news.slice(begin, end);
	});
}])