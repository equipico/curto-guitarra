app.controller('ProductsListCtrl', ['$scope', '$http', '$stateParams', '$sce', function ($scope, $http, $stateParams, $sce) {
	$scope.products = [];

	$http({
		method: 'GET',
		url: api_url+'?action=get_produtos',
	}).then(function successCallback(response){
		$scope.products = response.data;
	},
	function errorCallback(response){
		console.log('Erro ao carregar os produtos...');
		console.log(response);
	})

}])