app.controller('CategoryCtrl', ['$scope','$http', '$stateParams', function ($scope, $http, $stateParams) {
	$scope.categoria = $stateParams.category;

	// pagination
	$scope.filteredNews = [];
	$scope.currentPage = 1;
	$scope.numPerPage = 3;
	$scope.maxSize = 5;

	$http({
		method: 'GET',
		url: api_url+'?action=get_term_by_slug&slug='+$stateParams.category,
	}).then(function successCallback(response){
		$scope.posts = response.data;

		$scope.$watch('currentPage + numPerPage', function() {
			var begin = (($scope.currentPage - 1) * $scope.numPerPage);
			var end = begin + $scope.numPerPage;

			$scope.filteredNews = $scope.posts.slice(begin, end);
		});
	},
	function errorCallback(response){
		console.log('Erro ao carregar os produtos...');
		console.log(response);
	})
}])