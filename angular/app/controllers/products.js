app.controller('ProductCtrl', ['$scope', '$http', '$stateParams', '$sce', function ($scope, $http, $stateParams, $sce) {
	$http({
		method: 'GET',
		url: api_url+'?action=get_post_by_slug&post_type=nossos_produtos&slug='+$stateParams.product,
	}).then(function successCallback(response){
		$scope.post = response.data;
		if ($scope.post) {
			$scope.post.post_content = $sce.trustAsHtml($scope.post.post_content);
		}
	},
	function errorCallback(response){
		console.log('Erro ao carregar o menu...');
		console.log(response);
	})
}])