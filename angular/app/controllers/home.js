app.controller('HomeCtrl', ['$scope','$http', function ($scope, $http) {
	$scope.news = [];

	$http({
		method: 'GET',
		url: api_url+'?action=get_news',
	}).then(function successCallback(response){
		$scope.news = response.data;
	},
	function errorCallback(response){
		console.log('Erro ao carregar os produtos...');
		console.log(response);
	})

	// pagination
	$scope.filteredNews = [];
	$scope.currentPage = 1;
	$scope.numPerPage = 3;
	$scope.maxSize = 5;

	$scope.$watch('currentPage + numPerPage', function() {
		var begin = (($scope.currentPage - 1) * $scope.numPerPage);
		var end = begin + $scope.numPerPage;

		$scope.filteredNews = $scope.news.slice(begin, end);
	});
}])