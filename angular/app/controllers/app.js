var app = angular.module('CurtoGuitarra',['ui.router', 'angularVideoBg', 'ngSanitize', 'ui.bootstrap']);
var api_url = window.location.hostname.indexOf('localhost') != -1 ? 'http://curto-guitarra.localhost/wp/wp-admin/admin-ajax.php' : 'http://curtoguitarra.com/wp/wp-admin/admin-ajax.php' ;
var blog_configuration = {};
var conheca_leandro = {};

app.run(['$rootScope', '$http', '$timeout', '$uibModal', '$state', '$stateParams', '$sce', function ($rootScope, $http, $timeout, $uibModal, $state, $stateParams, $sce) {
	setTimeout(function(){

		if (!blog_configuration.tipo_topo) {
			$http({
				method: 'GET',
				url: api_url+'?action=get_blog_configuration',
			}).then(function successCallback(response){
				blog_configuration = response.data;
				$rootScope.blog_configuration = blog_configuration;
				if($rootScope.blog_configuration.call_to_action_form_code){
					$rootScope.blog_configuration.call_to_action_form_code = $sce.trustAsHtml($rootScope.blog_configuration.call_to_action_form_code);
				}
			},
			function errorCallback(response){
				console.log('Erro ao carregar as configurações do Blog...');
				console.log(response);
			})
		}

		if (!conheca_leandro.post_title) {
			$http({
				method: 'GET',
				url: api_url+'?action=get_conheca_leandro',
			}).then(function successCallback(response){
				conheca_leandro = response.data;
				$rootScope.conheca_leandro = conheca_leandro;
			},
			function errorCallback(response){
				console.log('Erro ao carregar conheça Leandro...');
				console.log(response);
			})
		}
		
	}, 700);

	$rootScope.open_search = function(){
		console.log()
		$uibModal.open({
			templateUrl: 'app/views/search.tpl.html',
			// controller: 'ModalInstanceCtrl'
	    })
	}
}])

app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
	// For any unmatched url, redirect to /
	$urlRouterProvider.otherwise("/");

	// Now set up the states
	$stateProvider
	.state('home', {
		url: "/",
		templateUrl: "app/views/home/index.tpl.html",
		controller: 'HomeCtrl'
	})
	.state('products-list', {
		url: "/produtos",
		templateUrl: "app/views/products-list/index.tpl.html",
		controller: 'ProductsListCtrl'
	})
	.state('products', {
		url: "/produtos/:product",
		templateUrl: "app/views/product/index.tpl.html",
		controller: 'ProductCtrl'
	})
	.state('news', {
		url: "/novidades/:news",
		templateUrl: "app/views/news/index.tpl.html",
		controller: 'NewsCtrl'
	})
	.state('search', {
		url: "/busca/:search",
		templateUrl: "app/views/search/index.tpl.html",
		controller: 'SearchCtrl'
	})
	.state('categories', {
		url: "/categorias/:category",
		templateUrl: "app/views/category/index.tpl.html",
		controller: 'CategoryCtrl'
	})
	.state('pages', {
		url: "/:page",
		templateUrl: "app/views/page/index.tpl.html",
		controller: 'PageCtrl'
	})

	// configure html5 to get links working on jsfiddle
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: true
	})
})

var menu_items = [];
app.controller('MenuCtrl', ['$scope', '$http', function ($scope, $http) {
	if (menu_items.length==0) {
		$http({
			method: 'GET',
			url: api_url+'?action=get_menu',
		}).then(function successCallback(response){
			menu_items = response.data;
			$scope.menu_items = menu_items;
		},
		function errorCallback(response){
			console.log('Erro ao carregar o menu...');
			console.log(response);
		})
	}

	$scope.menu_items = menu_items;
}])

app.controller('SlideCtrl', ['$scope', '$http', function ($scope, $http) {
	$scope.slides = [];

	$http({
		method: 'GET',
		url: api_url+'?action=get_slides',
	}).then(function successCallback(response){
		$scope.slides = response.data;
		setTimeout(function(){
			$('#slide-full').owlCarousel({
				items:1,
				margin:0,
				nav: false,
				loop: true,
				autoplay:false,
				autoplayTimeout:5000,
				loop:true
			});
		}, 500);
	},
	function errorCallback(response){
		console.log('Erro ao carregar os slides...');
		console.log(response);
	})
}])

app.controller('SlideNewsCtrl', ['$scope', '$http', function ($scope, $http) {
	$http({
		method: 'GET',
		url: api_url+'?action=get_news&posts_per_page=6',
	}).then(function successCallback(response){
		$scope.slide_news = response.data;
		setTimeout(function(){
			$('#slide-news').owlCarousel({
				items:1,
				margin:0,
				nav: false,
				loop: true,
				autoplay:false,
				autoplayTimeout:5000,
				loop:true
			});
		}, 500);
	},
	function errorCallback(response){
		console.log('Erro ao carregar os posts do slide...');
		console.log(response);
	})
}])

app.controller('SlideWithoutTextCtrl', ['$scope', '$http', function ($scope, $http) {
	$http({
		method: 'GET',
		url: api_url+'?action=get_slides_without_text',
	}).then(function successCallback(response){
		$scope.slide_news = response.data;
		setTimeout(function(){
			$('#slide-without-text-full').owlCarousel({
				items:1,
				margin:0,
				nav: false,
				loop: true,
				autoplay:false,
				autoplayTimeout:5000,
				loop:true
			});
		}, 500);
	},
	function errorCallback(response){
		console.log('Erro ao carregar os posts do slide...');
		console.log(response);
	})
}])

app.controller('SeachModalCtrl', ['$scope', '$state', '$uibModal', function ($scope, $state, $uibModal) {
	$scope.search_string = '';
	$scope.submit_search = function(){
		$state.go('search', {search: $scope.search_string})
	}
}])

app.controller('CategoriesCtrl', ['$scope', '$http', function ($scope, $http) {
	$http({
		method: 'GET',
		url: api_url+'?action=get_news_categories',
	}).then(function successCallback(response){
		$scope.categories = response.data;
	},
	function errorCallback(response){
		console.log('Erro ao carregar as categorias...');
		console.log(response);
	})
}])