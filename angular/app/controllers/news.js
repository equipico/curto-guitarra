app.controller('NewsCtrl', ['$scope', '$http', '$stateParams', '$sce', function ($scope, $http, $stateParams, $sce) {
	$http({
		method: 'GET',
		url: api_url+'?action=get_post_by_slug&post_type=post&slug='+$stateParams.news,
	}).then(function successCallback(response){
		$scope.post = response.data;
		if ($scope.post) {
			$scope.post.post_content = $sce.trustAsHtml($scope.post.post_content);
		}
	},
	function errorCallback(response){
		console.log('Erro ao carregar o menu...');
		console.log(response);
	})
}])